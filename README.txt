CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ

INTRODUCTION
------------
EZDFP Drupal module simplifies the process of adding Google DFP ads to your Drupal websites.  EZDFP is designed for non-programmers
requiring only basic knowledge of how to install a Drupal module.  Once installed configurations are done via your EZDFP.com account.
EZDFP provides management of ad inventory, ad campaigns, ad targeting.  All management and configuration is done via our web-based tool.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/ezdfp

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/ezdfp

REQUIREMENTS
------------
To use the EZDFP Drupal module, the following are required:

 * Google DFP accout
 * EZDFP.com account.  Sign up for FREE at ezdfp.com
 * Drupal website
 * DFP ads to display on your Drupal website

INSTALLATION
------------
 * Download and Install the ezdfp-widget module at download.ezdfp.com/drupal
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * All files and documentions are included in the download
 
 CONFIGURATION
--------------
 * Connect your EZDFP Drupal module to your EZDFP account:
 
   - Go to Configuration > EZDFP Adunit
   
   - Enter Email & Password to your EZDFP account and click Save Configuration. This enables your Drupal module to link to EZDFP.
   
 * Create EZDFP Block in your Drupalsite:

   - Select EZDFP from Main menu
   
   - Enter Block title, EZDFP title & select Adunit from the dropdown list. Then click Save
   
 * Enable the EZDFP block to be displayed on your webpage.

   - Go to EZDFP menu and click on the Blocks administration link
   
   - Find the EZDFP block you created and select from the REGION pull-down menu to assign the EZDFP block to that region
   
 * Finish and test.  It may take up to 72 hours for Google to start delivering ads to your site.

FAQ
---
* Do I have to have a Google DFP account to use EZDFP?  Yes
* Do I have to have a Drupal site to use this EZDFP Drupal module?  yes
* Is the pricing plan FREE?  yes EZDFP is free for use with one Drupal site
* Do I need to know programming to use EZDFP? no
* Is my DFP data saved in my Google DFP account?  yes
* Can I manage ad campaigns from my EZDFP account? yes
* Can I manage creatives from my EZDFP account? yes
